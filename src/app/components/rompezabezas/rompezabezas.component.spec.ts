import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RompezabezasComponent } from './rompezabezas.component';

describe('RompezabezasComponent', () => {
  let component: RompezabezasComponent;
  let fixture: ComponentFixture<RompezabezasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RompezabezasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RompezabezasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
