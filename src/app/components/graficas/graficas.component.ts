import {
  Component,
  Input,
  OnInit
} from '@angular/core';
import {
  Chart,
  registerables
} from 'chart.js';


@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.css']
})
export class GraficasComponent implements OnInit {

  nombres = ["Adrián", "Agustín", "Alberto", "Alejandro", "Alexander", "Alexis", "Alonso", "Andrés Felipe", "Ángel", "Anthony", "Antonio", "Bautista", "Benicio", "Benjamín", "Carlos", "Carlos Alberto", "Carlos Eduardo", "Carlos Roberto", "César", "Cristóbal", "Daniel", "David", "Diego", "Dylan", "Eduardo", "Emiliano", "Emmanuel", "Enrique", "Erik", "Ernesto", "Ethan", "Fabián", "Facundo", "Felipe", "Félix", "Félix María", "Fernando", "Francisco", "Francisco Javier", "Gabriel", "Gaspar", "Gustavo Adolfo", "Hugo", "Ian", "Iker", "Isaac", "Jacob", "Javier", "Jayden", "Jeremy", "Jerónimo", "Jesús", "Jesús Antonio", "Jesús Víctor", "Joaquín", "Jorge", "Jorge  Alberto", "Jorge Luis", "José", "José Antonio", "José Daniel", "José David", "José Francisco", "José Gregorio", "José Luis", "José Manuel", "José Pablo", "Josué", "Juan", "Juan Ángel", "Juan Carlos", "Juan David", "Juan Esteban", "Juan Ignacio", "Juan José", "Juan Manuel", "Juan Pablo", "Juan Sebastián", "Julio", "Julio Cesar", "Justin", "Kevin", "Lautaro", "Liam", "Lian", "Lorenzo", "Lucas", "Luis", "Luis Alberto", "Luis Emilio", "Luis Fernando", "Manuel", "Manuel Antonio", "Marco Antonio", "Mario", "Martín", "Mateo", "Matías", "Maximiliano", "Maykel", "Miguel", "Miguel  ngel", "Nelson", "Noah", "Oscar", "Pablo", "Pedro", "Rafael", "Ramón", "Raúl", "Ricardo", "Rigoberto", "Roberto", "Rolando", "Samuel", "Samuel David", "Santiago", "Santino", "Santos", "Sebastián", "Thiago", "Thiago Benjamín", "Tomás", "Valentino", "Vicente", "Víctor", "Víctor Hugo"];
  nombresAUtilizar = [];
  numerosAUtilizar= [];

  
  @Input() data = {
    numerosAUtilizar: []
  }


  constructor() {
    Chart.register(...registerables);
  }

  ngOnInit(): void {
    this.cracionDeNombres();
    this.creacionDeNumeros(this.nombresAUtilizar)
    this.cracionGrafica();
    console.log(this.nombresAUtilizar);
    console.log(this.numerosAUtilizar);
    


  }

  cracionGrafica() {

    var myChart = new Chart("myChart", {
      type: 'bar',
      data: {
        labels: this.nombresAUtilizar,
        datasets: [{

          label: '# of Votes',
          data: this.numerosAUtilizar,
          minBarLength: 2,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
            ticks: {
              stepSize: 1,
              // count: 1,
              maxTicksLimit: 50
            }
          }
        },
        plugins: {
          tooltip: {
            enabled: false,
          },
          legend: {
            display: false,
          }
        },

      },

    });
  }

  cracionDeNombres() {
    //Necesita, Nombre(parte abajo), Rango(Para la graficacion), Maximo de personas(5), minimo(2)
    let randomnumeros = this.getRandomInt(2, 6)

    for (let i = 0; i < randomnumeros; i++) {
      let randomNombre = Math.floor(Math.random() * this.nombres.length);

      this.nombresAUtilizar.push(this.nombres[randomNombre])

    }
    return this.nombresAUtilizar


  }

  creacionDeNumeros(nombres){
      for (let i = 0; i < nombres.length; i++) {
        let cantidadXNombre = this.getRandomInt(1,30);
        this.numerosAUtilizar.push(cantidadXNombre);

      }
      return this.numerosAUtilizar;
      console.log(this.numerosAUtilizar);
      
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  creacionObjRadioBtn(objData: any){

  }

}
