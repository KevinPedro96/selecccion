import {
  Component,
  Input,
  OnInit,
  OnChanges
} from '@angular/core';
import Phaser from 'phaser';

var enviar;
var enviar2
var size = [700];
var data;
var rotacion = 0;


class myScene extends Phaser.Scene {
  preload() {
    this.load.image('punteroRegla', 'assets/Regla/ReglaPuntero.png');
    this.load.image("mover+", "./assets/mover+.png")
    this.load.image("mover-", "./assets/mover-.png")
  }

  create() {



    if (data.tipoFig === 1) {
      // Creacion del circulo
      var graphics = this.add.graphics({
        lineStyle: {
          width: 2,
          color: 0x00ff00
        },
        fillStyle: {
          color: 0xff0000
        }
      });

      var circle = new Phaser.Geom.Circle(size[0] / 2, size[0] / 2, data.diametro * 10);

      graphics.fillCircleShape(circle);


    } else if (data.tipoFig === 2) {
      // Creacion del Triangulo
      var graphics = this.add.graphics({
        lineStyle: {
          width: 5,
          color: 0x0000ff
        }
      });

      var triangle = Phaser.Geom.Triangle.BuildEquilateral(size[0] / 2, size[0] / 2 / 2, data.diametro * 20 - 5);
      console.log(triangle);


      graphics.strokeTriangleShape(triangle);

    } else if (data.tipoFig === 3) {
      // Creacion del Cuadrado

      var graphics = this.add.graphics({
        fillStyle: {
          color: 0x0000aa
        }
      });
      var rect = new Phaser.Geom.Rectangle(0, 0, data.diametro * 20, data.diametro * 20);
      rect.centerX = size[0] / 2;
      rect.centerY = size[0] / 2;
      graphics.fillRectShape(rect);


      // graphics.lineStyle(10, 0xaa0200);
      // graphics.lineBetween(rect.centerX - 10, rect.centerY, rect.centerX + 10, rect.centerY);
      // graphics.lineBetween(rect.centerX, rect.centerY - 10, rect.centerX, rect.centerY + 10);


    }

    // var line = new Phaser.Geom.Line(0, size[0] / 2, 700, size[0] / 2);

    // var graphics = this.add.graphics({
    //   lineStyle: {
    //     width: 4,
    //     color: 0xaa00aa
    //   }
    // });

    // var line2 = new Phaser.Geom.Line(size[0] / 2, 0, size[0] / 2, 700);



    // graphics.strokeLineShape(line);
    // graphics.strokeLineShape(line2);

    //Creacion de Regla

    var blitter = this.add.image(20, 0, 'punteroRegla').setScale(0.420);

    this.input.on('pointermove', function (pointer) {

      blitter.x = pointer.x;
      blitter.y = pointer.y;
    });
    enviar = this.add.image(100, 100, "mover-").setInteractive();
    enviar.setScale(0.200)
    enviar.setTint(0xEED715)


    enviar.on('pointerdown', function () {
      rotacion = rotacion + .050;
      blitter.setRotation(rotacion);
    })

    enviar2 = this.add.image(600, 100, "mover+").setInteractive();
    enviar2.setScale(0.200)
    enviar2.setTint(0xEED715)


    enviar2.on('pointerdown', function () {
      rotacion = rotacion - .050;
      blitter.setRotation(rotacion);
    })


  }


  update() {

  }
}


@Component({
  selector: 'app-regla',
  templateUrl: './regla.component.html',
  styleUrls: ['./regla.component.css']
})
export class ReglaComponent implements OnInit, OnChanges {

  phaserGame: Phaser.Game;
  config: Phaser.Types.Core.GameConfig;

  selectedValue: string;

  respuestaCorrecta;

  options = [];

  @Input() dataInput = {
    diametro: 14,
    tipoFig: 1,
  };

  constructor() {
    this.config = {
      type: Phaser.AUTO,
      width: size[0],
      height: size[0],
      parent: "game",
      backgroundColor: '#CBD0D7',
      scene: [myScene],
      physics: {
        default: "arcade",
        arcade: {
          gravity: {
            y: 500
          }
        }
      }
    }

  }

  ngOnInit(): void {
    this.phaserGame = new Phaser.Game(this.config);
    data = this.dataInput;
    console.log(data);
    this.genarateOpctionSelect();
    console.log(this.options);
    console.log(this.selectedValue);
    
    
  }
  
  ngOnChanges(): void {
    this.comprobacion();
    
  }

  genarateOpctionSelect() {
    let convert = this.dataInput.diametro / 2;
    let bandera = false;
    let contador = 0;
    let pos = this.getRandomArbitrary(0, 3);

    let correctAnswer = {
      value: 0,
      viewValue: 0,
      flag: false
    };
    let incorrectAnswer = {
      value: 0,
      viewValue: 0,
      flag: false
    };

    while (!bandera) {
      if (contador === pos) {
        correctAnswer.viewValue = convert;
        correctAnswer.value = convert;
        correctAnswer.flag = true;
        this.options.push(correctAnswer);
        contador++;
      }
      let randomAnswer = this.getRandomArbitrary(1, 30);

      randomAnswer = randomAnswer / 2;
      // console.log(randomAnswer);
      if (this.options === []) {
        this.options.push({
          value: randomAnswer,
          viewValue: randomAnswer,
        });
        contador++;
      } else {
        let buscador = this.options.find(element => element.value == randomAnswer);
        if (buscador === undefined) {
          this.options.push({
            value: randomAnswer,
            viewValue: randomAnswer,
            flag: false
          });
          contador++;
        }
      }
      if (this.options.length === 4) {
        bandera = true;
      }



    }


  }

  comprobacion() {
    
    console.log(this.options);    

    if(this.selectedValue == undefined){
      return;
    }else{
      
      if(Number( this.dataInput.diametro/2) === Number( this.selectedValue)){
        return
      }
      else{
        
      }
    }

      


  }

  getRandomArbitrary(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  }

}
