import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionTraignulosComponent } from './seleccion-traignulos.component';

describe('SeleccionTraignulosComponent', () => {
  let component: SeleccionTraignulosComponent;
  let fixture: ComponentFixture<SeleccionTraignulosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionTraignulosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionTraignulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
