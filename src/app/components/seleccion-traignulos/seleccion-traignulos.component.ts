import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-seleccion-traignulos',
  templateUrl: './seleccion-traignulos.component.html',
  styleUrls: ['./seleccion-traignulos.component.css']
})
export class SeleccionTraignulosComponent implements OnInit {

  // ../../../../assets/SeleccionK

  @Output() dataSend: EventEmitter<any> = new EventEmitter();
  // @Input() onlyView: boolean = true;
  // @Input() viewResult: boolean;
  // @Input() data: any;
  @Input() askForResult = false;
  @Input() infoRes: any;
  @Input() onlyView: boolean = true;
  @Input() viewResult: boolean = false;
  @Input() data: any = {
    description: "Selecciones la figuras que sean un circulo",
    no: [
      {no: 7},
      {no: 14},
      {no: 5},
      {no: 18},
      {no: 6},
      {no: 15},
    ],
    typeFig: 3,
    
  }
  array = [];
  totalAswer = {
    total: 6,
    correct: 0,
    incorrect: 0,
  }
  puntos: any; 
  nameTriangule = "";
  randomTringule = 2 
  // Math.floor(Math.random() * 3) + 1;

//  array =  [
//     {
//         "id": 0,
//         "no": 7,
//         "src": "./assets/SeleccionK/Circulo/7.png",
//         "answer": "Circulo",
//         "select": true
//     },
//     {
//         "id": 1,
//         "no": 14,
//         "src": "./assets/SeleccionK/IncorrectasIMG/14.png",
//         "answer": "",
//         "select": true
//     },
//     {
//         "id": 2,
//         "no": 5,
//         "src": "./assets/SeleccionK/Circulo/5.png",
//         "answer": "Circulo",
//         "select": false
//     },
//     {
//         "id": 3,
//         "no": 18,
//         "src": "./assets/SeleccionK/IncorrectasIMG/18.png",
//         "answer": "",
//         "select": false
//     },
//     {
//         "id": 4,
//         "no": 6,
//         "src": "./assets/SeleccionK/Circulo/6.png",
//         "answer": "Circulo",
//         "select": true
//     },
//     {
//         "id": 5,
//         "no": 15,
//         "src": "./assets/SeleccionK/IncorrectasIMG/15.png",
//         "answer": "",
//         "select": true
//     }
// ]
  

  constructor() {}
  
  ngOnInit(): void {
    this.getObjFig();
    this.getfigure(this.data.typeFig);    
    // this.pushTriangule();
    
  }

  ngOnChanges(): void {
    // if(this.askForResult){
    //     this.enviar();
    // }
    // else{
    //   this.array = [];
    //   if(!this.viewResult ) this.getObjFig();
    //   else {
    //     this.data = this.infoRes;
    //     this.array = this.data.figure;
    //   }
    // }
      
  }

  getfigure(type: any){
    
    if(this.data.typeFig === 1 ){
      return type = "Circulo";
    }else if(this.data.typeFig === 2){
      return type = "Cuadrado";
    }else if (this.data.typeFig === 3){
      if(this.randomTringule === 1){
       this.nameTriangule =  "escaleno"
      }else if (this.randomTringule === 2){
        this.nameTriangule = "rectangulo"
      }else if (this.randomTringule === 3){
        this.nameTriangule = "equilatero"
      }
      return type = "Triangulos";
    }
    
  } 

  getObjFig(){
   
    this.array = [];
    let figura;
    
    for (let i = 0; i < 6; i++) {
      let cheked = this.checkDefigure(this.data.no[i].no, this.getfigure(this.data.typeFig));
      if(this.data.no[i].no <= 9){
        figura = {
          id: i,
          no: this.data.no[i].no,
          src: `../../../../assets/SeleccionK/${this.getfigure(this.data.typeFig)}/${this.data.no[i].no}.png`,
          answer: cheked,
          select: false,
        }
        this.array.push(figura);
      }else{
        if(this.data.typeFig === 3){
          if(this.randomTringule === 1){
            figura = {
              id: i,
              no: this.data.no[i].no,
              src: `../../../../assets/SeleccionK/Triangulos/escalenos/${this.data.no[i].no}.png`,
              answer: cheked,
              select: false,
            }
            this.array.push(figura)
          }
        }else{

          // figura = {
          //   id: i,
          //   no: this.data.no[i].no,
          //   src: `../../../../assets/SeleccionK/IncorrectasIMG/${this.data.no[i].no}.png`,
          //   answer: cheked,
          //   select: false,
          //   // puntos: correct + '/' + total
          // }
          // this.array.push(figura)
        }
      }
    }
 
    // console.log(this.array);
    console.log(this.array);
  }

  // pushTriangule(){
  //   if(this.data.typeFig === "Triangulos"){
  //     for (let i = 0; i < this.array.length; i++) {
  //       this.array[i].src = `../../../../assets/SeleccionK/Triangulos/Equilateros/7.png`
        
  //     }
  //   }
    
  // }

  enviar() {
    
    let contadCorrect = 0, contadIncorrect = 0;
    
    for (const iterator of this.array) {
      if (iterator.select) {
        if (iterator.answer !== "") contadCorrect++;
        else contadIncorrect++;
        // iterator.totalAswer.correct = contadCorrect;
        // iterator.totalAswer.incorrect = contadIncorrect;
      }
    }
    this.totalAswer.correct = contadCorrect;
    this.totalAswer.incorrect = contadIncorrect;
    
    this.puntos =  this.totalAswer.correct + '/' + this.data.no.length;

    this.data.figure = this.array;
    this.data.puntos = this.puntos;
    this.dataSend.emit(this.data);

  }

  getSelected(index) {
    if (this.onlyView) return;
    this.array[index].select = !this.array[index].select;

  }
  
  checkDefigure(check: number, type: string) {
    if (type === this.getfigure(this.data.typeFig)) {
      if (check === 1 || check === 2 || check === 3 || check === 4 || check === 5 || check === 6 || check === 7 || check === 8 || check === 9 ) {
        // this.totalFigure ++;
        return type;
      } else return "";
    }
  }

  
  backgroundColorCheck(select: boolean, answer: string){
    if(select && answer !== '') return 'green';
    else if(select && answer === '') return 'red';
  }


}
