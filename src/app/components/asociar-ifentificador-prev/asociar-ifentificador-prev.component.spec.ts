import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsociarIfentificadorPrevComponent } from './asociar-ifentificador-prev.component';

describe('AsociarIfentificadorPrevComponent', () => {
  let component: AsociarIfentificadorPrevComponent;
  let fixture: ComponentFixture<AsociarIfentificadorPrevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsociarIfentificadorPrevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsociarIfentificadorPrevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
