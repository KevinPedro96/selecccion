import { Component, Input, OnInit, OnChanges, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-asociar-ifentificador-prev',
  templateUrl: './asociar-ifentificador-prev.component.html',
  styleUrls: ['./asociar-ifentificador-prev.component.css']
})
export class AsociarIfentificadorPrevComponent implements OnInit, OnChanges {

  // ../../../../assets/SeleccionK

  @Output() dataSend: EventEmitter<any>
  @Input() onlyView: boolean;
  // @Input() viewResult: boolean;
  @Input() typeFigure;
  // @Input() data: any;
  // @Input() onlyView: boolean = false;
  @Input() viewResult: boolean = true;
  // @Input() typeFigure;
  // @Input() data: any = {
  //   generateRandom: true,
  //   name: 'Titlulo',
  //   description: 'Descripción',
  //   type: 'Circulo',
  //   figure: [],
  //   totalAswer: {
  //     total: 0,
  //     correct: 0,
  //     incorrect: 0
  //   },
  //   totalFig: 6,
  // }
  @Input() data: any = {
    description: "Descripción",
    figure: [
      { id: 0, no: 2, src: "./assets/SeleccionK/Circulo/2.png", answer: "Circulo", select: true },
      { id: 1, no: 3, src: "./assets/SeleccionK/Circulo/3.png", answer: "Circulo", select: true },
      { id: 2, no: 15, src:"./assets/SeleccionK/IncorrectasIMG/15.png", answer: "", select: true },
      { id: 3, no: 18, src:"./assets/SeleccionK/IncorrectasIMG/18.png", answer: "", select: true },
      { id: 4, no: 12, src:"./assets/SeleccionK/IncorrectasIMG/12.png", answer: "", select: false },
      { id: 5, no: 1, src: "./assets/SeleccionK/Circulo/1.png", answer: "Circulo", select: true },
    ],
    generateRandom: false,
    name: "Titlulo",
    totalAswer: { total: 3, correct: 2, incorrect: 1 },
    totalFig: 6,
    type: "Circulo"
  }

  array = [];
  totalFigure: number;  

  constructor() {
    this.dataSend = new EventEmitter();
  }

  ngOnChanges(): void {

    this.objetFig(this.data.type);
  }

  ngOnInit(): void {

    if(this.data.generateRandom){
      this.objetFig(this.data.type);
      console.log(this.data);
    }

  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  }


  objetFig(tipo) {
    this.totalFigure = 0;
    this.array = [];

    let figura, numfig, file;
    let correctImg = 0;
    for (let i = 0; i < 6; i++) {
      numfig = this.getRandomInt(1, 18);

      while (this.array.find(element => element.no === numfig) !== undefined) {
        if (numfig == 6) numfig = 1;
        else numfig = numfig + 1;
      }
      
      let cheked = this.checkDefigure(numfig, tipo);
      if(numfig <= 9){
        file = tipo;
        correctImg++;
      } 
      else file = 'IncorrectasIMG'

      figura = {
        id: i,
        no: numfig,
        src: `../../../../assets/SeleccionK/${file}/${numfig}.png`,
        answer: cheked,
        select: false
      }
      this.array.push(figura);


    }
    if(correctImg < 3 ) {
      let pos = this.getRandomInt(1,6);
      numfig = this.getRandomInt(1,9);
      while(this.array[pos].no <= 9 ){
        pos = this.getRandomInt(1,6);
      }
      this.totalFigure ++;
      this.array[pos].no = numfig;
      this.array[pos].src = `../../../../assets/SeleccionK/${tipo}/${numfig}.png`
    }else if(correctImg === 6){
      this.totalFigure--;
      let pos = this.getRandomInt(1,6);
      numfig = this.getRandomInt(10,18);
      this.array[pos].no = numfig;
      this.array[pos].src = `../../../../assets/SeleccionK/IncorrectasIMG/${numfig}.png`
    }
    this.data.figure = this.array;
    this.data.totalAswer.total = this.totalFigure;
    console.log(this.array);

  }

  enviar() {
    let contadCorrect = 0, contadIncorrect = 0;

    for (const iterator of this.data.figure) {
      if (iterator.select) {
        if (iterator.answer !== "") contadCorrect++;
        else contadIncorrect++;
      }
    }
    this.data.totalAswer.correct = contadCorrect;
    this.data.totalAswer.incorrect = contadIncorrect;

    console.log(this.data);
    this.dataSend.emit(this.data);
  }

  getSelected(index) {
    if (this.onlyView) return;
    this.data.figure[index].select = !this.data.figure[index].select;

  }

  checkDefigure(check: number, type: string) {
    if (type === 'Circulo' || type === 'Cuadrado' || type === 'Triangulos') {
      if (check === 1 || check === 2 || check === 3 || check === 4 || check === 5 || check === 6 || check === 7 || check === 8 || check === 9 ) {
        this.totalFigure ++;
        return type;
      } else return "";
    }
  }

  backgroundColorCheck(select: boolean, answer: string){
    if(select && answer !== '') return 'Green';
    else if(select && answer === '') return 'red';
  }

}
