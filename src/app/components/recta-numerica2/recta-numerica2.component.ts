import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-recta-numerica2',
  templateUrl: './recta-numerica2.component.html',
  styleUrls: ['./recta-numerica2.component.css'],
})
export class RectaNumerica2Component implements OnInit {
  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  mitad: any;

  timePeriods = [
    { value: '1', disabled: true },
    { value: '', disabled: false },
    { value: '3', disabled: true },
    { value: '', disabled: false },
    { value: '5', disabled: true },
  ];

  resp = [
    {value: '2'},
    {value: '4'}
  ]
  
  answers: string[][] = [[], [], [], []];
  

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  ngAfterViewInit(): void {}

  constructor() {}

  ngOnInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    if (this.ctx) {
      this.creacionRecta();
    }
  }

  creacionRecta() {
    this.mitad = {
      x: 375,
      y: 15,
      w: 2,
      h: 10,
    };

    //stilos
    this.ctx.lineWidth = 5;
    this.ctx.strokeStyle = 'black';

    // trazo
    this.ctx.beginPath();
    this.ctx.moveTo(50, 15);
    this.ctx.lineTo(700, 15);
    this.ctx.stroke();

    //Trazar flechas
    this.ctx.beginPath();
    this.ctx.moveTo(20, 15);
    this.ctx.lineTo(50, 7);
    this.ctx.lineTo(50, 23);
    this.ctx.fill();

    this.ctx.beginPath();
    this.ctx.moveTo(730, 15);
    this.ctx.lineTo(700, 7);
    this.ctx.lineTo(700, 23);
    this.ctx.fill();

    //crear lineas divisoras 2
    this.ctx.fillRect(this.mitad.x, this.mitad.y, this.mitad.w, this.mitad.h);
    // crear lineas divisoras 3
    // this.ctx.fillRect(this.mitad.x/2, this.mitad.y, this.mitad.w, this.mitad.h);
    // this.ctx.fillRect(this.mitad.x+ 187.5, this.mitad.y, this.mitad.w, this.mitad.h)
    // crear lineas divisoras 4
    this.ctx.fillRect(
      this.mitad.x / 3,
      this.mitad.y,
      this.mitad.w,
      this.mitad.h
    );
    this.ctx.fillRect(
      this.mitad.x - 100,
      this.mitad.y,
      this.mitad.w,
      this.mitad.h
    );
    this.ctx.fillRect(
      this.mitad.x + 100,
      this.mitad.y,
      this.mitad.w,
      this.mitad.h
    );
    this.ctx.fillRect(
      this.mitad.x + 220,
      this.mitad.y,
      this.mitad.w,
      this.mitad.h
    );
  }
}
