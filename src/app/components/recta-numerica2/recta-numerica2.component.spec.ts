import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RectaNumerica2Component } from './recta-numerica2.component';

describe('RectaNumerica2Component', () => {
  let component: RectaNumerica2Component;
  let fixture: ComponentFixture<RectaNumerica2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RectaNumerica2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RectaNumerica2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
