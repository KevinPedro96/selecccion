import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsociarIdentificadorComponent } from './asociar-identificador.component';

describe('AsociarIdentificadorComponent', () => {
  let component: AsociarIdentificadorComponent;
  let fixture: ComponentFixture<AsociarIdentificadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsociarIdentificadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsociarIdentificadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
