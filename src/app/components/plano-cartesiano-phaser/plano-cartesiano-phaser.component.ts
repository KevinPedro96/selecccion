import {
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import Phaser from 'phaser';

class ImagenesForma {
  x: number;
  y: number;
  w: number;
  h: number;
  image: any;

  ctx: CanvasRenderingContext2D;

  constructor(x: number, y:number, w: number, h: number, image: any, ctx: CanvasRenderingContext2D){
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.image = image;
      this.ctx = ctx;
  }

  dibuja(){
    this.ctx.save();

    if(this.image){
      this.ctx.drawImage(this.image, this.x,this.y, this.h, this.w);
    }
    this.ctx.restore();
    console.log(this.y,"Esto es y");
    console.log(this.x, "Esto es x");
    
  }
}

var _data;
var imagenes: any[];
var punto = [];
var count = 0;

class myScene extends Phaser.Scene {


  imagen = [
    'carrito',
    'mochila',
    'corazon',
    'lapiz',
    'manzana'
  ];

  constructor() {
    super({
      key: 'main'
    })
  }

  preload() {
    // for (let item = 0; item < this.imagen.length; item++) {

    //   this.load.image('imagenPre',this.imagen[item]);

    // }
    this.load.image('carrito', 'assets/FraccionesPropiasKev/Dibujo0.png');
    this.load.image('mochila', 'assets/FraccionesPropiasKev/Dibujo1.png');
    this.load.image('corazon', 'assets/FraccionesPropiasKev/Dibujo2.png');
    this.load.image('lapiz', 'assets/FraccionesPropiasKev/Dibujo3.png');
    this.load.image('manzana', 'assets/FraccionesPropiasKev/Dibujo4.png');
  }



  create() {
    var x = [50, 100, 151, 202, 253, 304, 355, 406, 457, 508, 559];
    var y = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550];

    _data = [{
        xPos: 4,
        yPos: 7
      },
      {
        xPos: 1,
        yPos: 9
      },
      {
        xPos: 9,
        yPos: 5
      }
    ];

    imagenes = [];
    imagenes = _data;

    console.log(imagenes);

    var graphics = this.add.graphics({
      lineStyle: {
        width: 4,
        color: 0x000000
      },
      fillStyle: {
        color: 0x000000
      }
    });

    let numero = -1;
    let numero2 = 11;
    for (let xpos = 50; xpos < 600; xpos++) {
      for (let ypos = 50; ypos < 600; ypos++) {
        var line = new Phaser.Geom.Line(48, xpos, 580, xpos);
        var line2 = new Phaser.Geom.Line(ypos, 550, ypos, 20);
        numero = numero + 1;
        numero2 = numero2 - 1;
        this.add.text(xpos, 560, numero.toString()).setFontFamily('Arial').setFontSize(20).setColor('0x000000');
        this.add.text(25, ypos - 20, numero2.toString()).setFontFamily('Arial').setFontSize(20).setColor('0x000000');
        ypos = ypos + 50;
        xpos = xpos + 50;
        graphics.strokeLineShape(line);
        graphics.strokeLineShape(line2);
      }
    }

    var line3 = new Phaser.Geom.Line(20, 592, 20, 30);
    var line4 = new Phaser.Geom.Line(20, 590, 580, 590);
    var triangle = new Phaser.Geom.Triangle(20, 10, 10, 30, 30, 30);
    var triangle2 = new Phaser.Geom.Triangle(590, 590, 570, 580, 570, 600);

    graphics.fillTriangleShape(triangle);
    graphics.fillTriangleShape(triangle2);
    graphics.strokeLineShape(line3);
    graphics.strokeLineShape(line4);


    for (let item of imagenes) {

      var xCor = x[item.xPos];
      var yCor = y[item.yPos];

      punto.push(this.add.image(xCor, yCor, this.imagen[count]).setInteractive());
      count++;

      console.log(xCor);
      console.log(yCor);

    }
    for (const iterator of punto) {
      iterator.setScale(0.09);
    }


    // var star = (this.add.image(xCor, yCor, 'star').setInteractive());
    // star.setScale(0.009);

    // var coincidencia = xPos;
    // xPos = Math.floor((Math.random() * x.length) + 0); 
    // yPos = Math.floor((Math.random() * y.length) + 0);
    // while(xPos === coincidencia ){
    //   xPos = Math.floor((Math.random() * x.length) + 0);  
    // }
    // xCor = x[xPos];
    // yCor = y[yPos];


    // this.imagen1.push(this.add.image(xCor, yCor,'star').setInteractive());
    // this.imagen1.setScale(0.009);

  }

  update() {}
}

@Component({
  selector: 'app-plano-cartesiano-phaser',
  templateUrl: './plano-cartesiano-phaser.component.html',
  styleUrls: ['./plano-cartesiano-phaser.component.css']
})
export class PlanoCartesianoPhaserComponent implements OnInit {

  imagenCargada = false; 
  imageForma: ImagenesForma[];
  varImagen;

  @ViewChild('canvas', {
    static: true
  })
  canvas: ElementRef < HTMLCanvasElement > ;
  private ctx: CanvasRenderingContext2D;


  phaserGame: Phaser.Game;
  config: Phaser.Types.Core.GameConfig;
  size = [600, 600];
  x; 
  y;
  imagenesFig = new Image();

  constructor() {
    this.config = {
      type: Phaser.AUTO,
      width: 600,
      height: 600,
      parent: "game",
      backgroundColor: '#CBD0D7',
      scene: [myScene],
      physics: {
        default: "arcade",
        arcade: {
          gravity: {
            y: 500
          }
        }
      }
    }
  }

  ngOnInit(): void {
    // this.phaserGame = new Phaser.Game(this.config);
    
    
  }
  
  ngAfterViewInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
   
    if (this.ctx) {
      
     
      this.cargarFiguras();
      this.creacionPlanoCartesiano();
      

    }
    
  }
  creacionPlanoCartesiano(){
 
      // Estilo de lineas
      this.ctx.lineWidth = 4;
      this.ctx.strokeStyle = "black"
      
      //Flechas
      this.ctx.beginPath();
      this.ctx.moveTo(20, 10);
      this.ctx.lineTo(10, 30);
      this.ctx.lineTo(30, 30);
      this.ctx.fill()

      this.ctx.beginPath();
      this.ctx.moveTo(590, 580);
      this.ctx.lineTo(570, 570);
      this.ctx.lineTo(570, 590);
      this.ctx.fill()

      this.ctx.save
      
      // Iniciar Trazos
      this.ctx.beginPath();
      this.ctx.moveTo(20, 20);
      this.ctx.lineTo(20, 580);
      this.ctx.stroke();

      this.ctx.beginPath();
      this.ctx.moveTo(18, 580);
      this.ctx.lineTo(580, 580);
      this.ctx.stroke();

      this.ctx.save()

      



      // Cuadricula 
      
      let numeroX = 11;
      for (this.x = 50; this.x <= this.size[0] -50; this.x = this.x + 50) {
        numeroX = numeroX - 1;
        this.ctx.moveTo(this.x, 50);
        this.ctx.lineTo(this.x, this.size[0] -50);
        this.ctx.font = "20px Arial";
        this.ctx.fillText(numeroX.toString() ,23, this.x +10);
        
        
      }

      let numeroY = -1;
      for (this.y = 50; this.y <= this.size[0] - 50; this.y = this.y + 50) {
        numeroY = numeroY + 1;
        this.ctx.moveTo(50, this.y);
        this.ctx.lineTo(this.size[0] - 50 , this.y);
        this.ctx.font = "20px Arial";
        this.ctx.fillText(numeroY.toString() ,this.y -5, 570);

        
      }

      this.ctx.stroke();

  }

  cargarFiguras(){

    // x= Fila, y= Columnas
    let posY = [540,490,440,390,340,290,240,190,140,90,40];
    let posX = [40,90,140,190,240,290,340,390,440,490,540];

    this.imageForma = []
    for (let i = 0; i < 3; i++) {
      let sacarX = this.getRandomArbitrary(0,10);
      let sacarY = this.getRandomArbitrary(0,10);
      console.log(sacarX,"Esto es sacarX");
      console.log(sacarY,"Esto es sacarY");
      
      
      let objetoImagen = new ImagenesForma(posX[sacarX] -5, posY[sacarY] -5, 30,30, undefined, this.ctx);   
      
      objetoImagen.image = new Image();
      
      objetoImagen.image.onload = () => {
        objetoImagen.dibuja(); 
        
      }
      objetoImagen.image.src = "assets/FraccionesPropiasKev/Dibujo"+i+".png"
      
      this.imageForma.push(objetoImagen)
      
      
    }
     

     
  }
  getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }


 

}
