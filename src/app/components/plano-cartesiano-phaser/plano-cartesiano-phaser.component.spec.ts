import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanoCartesianoPhaserComponent } from './plano-cartesiano-phaser.component';

describe('PlanoCartesianoPhaserComponent', () => {
  let component: PlanoCartesianoPhaserComponent;
  let fixture: ComponentFixture<PlanoCartesianoPhaserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanoCartesianoPhaserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanoCartesianoPhaserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
