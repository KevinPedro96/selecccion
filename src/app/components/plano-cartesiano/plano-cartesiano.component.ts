import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';


class Numero {
  x: number;
  y: number;
  w: number;
  h: number;

  id: number;

  ctx: CanvasRenderingContext2D;
  valido: boolean = true;

  constructor(id: number, x: number, y: number, w: number, h: number, numero: any, ctx: CanvasRenderingContext2D) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.id = id;

    this.ctx = ctx;
  }

  dibuja() {
    this.ctx.save();
    if (this.valido === false) {
      return;
    } else {
      this.ctx.fillStyle = "green";
      this.ctx.lineWidth = 3;
      var X = 0;
      var Y = 0;
      var R = 10;

      var L = 5;
      var paso = 2;

      var estrella = L / paso;
      var rad = (2 * Math.PI) / estrella;

      this.ctx.translate(this.x, this.y);

      // dibuja el trazado 
      this.ctx.beginPath();
      for (var i = 0; i < L; i++) {
        let x = X + R * Math.cos(rad * i);
        let y = Y + R * Math.sin(rad * i);
        this.ctx.lineTo(x, y);
      }
      this.ctx.closePath();
      this.ctx.stroke();
      this.ctx.fill();
      

      this.ctx.restore();
    }
  }
  comprueba(x, y) {
    if (x > (this.x - this.w / 2) && x < (this.x - this.w / 2) + this.w && y > (this.y - this.h / 2) && y < (this.y - this.h / 2) + this.h) {
      return true;
    } else {
      return false;
    }
  }
}

class Palito {
  posX: number;
  posY: number;
  numero: number;
  h: number;
  w: number;
  numOculto: boolean = false;
  punto: boolean;
  ctx: CanvasRenderingContext2D;

  constructor(x: number, y: number, numero: number, h: number, w: number, numOculto: boolean, punto: boolean, ctx: CanvasRenderingContext2D) {
    this.posX = x;
    this.posY = y;
    this.numero = numero;
    this.h = h;
    this.w = w;
    this.numOculto = numOculto;
    this.punto = punto;
    this.ctx = ctx;
  }

  dibuja() {
    if (this.numOculto === true) {
      return;
    } else {

      this.ctx.save();
      this.ctx.lineWidth = 2;
      this.ctx.strokeStyle = 'black';

      this.ctx.beginPath();
      this.ctx.moveTo(this.posX, 590);
      this.ctx.lineTo(this.posX, 560);
      this.ctx.stroke();
      this.ctx.restore();

      this.ctx.font = "20px Verdana";
      this.ctx.textBaseline = 'middle';
      this.ctx.textAlign = 'center';
      this.ctx.fillText(this.numero.toString(), this.posX, this.posY + 45);

    }
  }

  comprueba(x, y) {
    if (x > (this.posX - this.w / 2) && x < (this.posX - this.w / 2) + this.w && y > (this.posY - this.h / 2) && y < (this.posY - this.h / 2) + this.h) {
      return true;
    } else {
      return false;
    }
  }


}

@Component({
  selector: 'app-plano-cartesiano',
  templateUrl: './plano-cartesiano.component.html',
  styleUrls: ['./plano-cartesiano.component.css']
})
export class PlanoCartesianoComponent implements OnInit {
  @ViewChild('canvas', {
    static: true
  })
  canvas: ElementRef < HTMLCanvasElement > ;
  private ctx: CanvasRenderingContext2D;


  @Input() data: any = {
    initLine: 0,
    endLine: 100,
    numerosAleatorios: [10, 50, 20, 75, 65],
    puntos: 0,
    numero: 0

  };
  @Input() tipoDeVista: any = {
    vistaPrevia: false,
    vistaResultado: false,
  }; // @Input() onlyView: boolean = false; @Input() viewResult: boolean = false;
  @Input() aksForResult: boolean = false;
  @Output() respuesta: EventEmitter < any > ;

  size = [600, 600];
  numero: Numero[] = [];
  palitos: Palito[] = [];
  // initLine = 0;
  // endLine = 100;
  // numerosAleatorios = [10, 50, 20, 75, 65, ];
  recta = {
    x: 50,
    y: 200,
    xx: 700,
    yy: 200
  };
  numeroSeleccionado: number = this.data.numerosAleatorios[0];

  constructor() {
    this.respuesta = new EventEmitter();
  }

  ngOnChanges(): void {
    if (this.aksForResult) {
      this.respuesta.emit(this.data);
    }
  }

  ngAfterViewInit(): void {

    let numxPixel = this.data.endLine - this.data.initLine;
    let cantidadPixeles = this.recta.xx - this.recta.x;
    let numPix = cantidadPixeles / numxPixel;
    this.canvas.nativeElement.addEventListener('click', this.selecciona.bind(this), false);
    this.ctx = this.canvas.nativeElement.getContext('2d')
    if (this.ctx) {   
      this.resetCanvas();
    }
  }

  ngOnInit() {

  }

  cuadricula() {

    this.ctx.save();
    
    for (let x = 0; x <= this.size[0]; x = x + 40) {

      this.ctx.moveTo(x, 0);
      this.ctx.lineTo(x, this.size[1] -40);
    }

    for (let y = 0; y <= this.size[1]; y = y + 40) {
      this.ctx.moveTo(40, y);
      this.ctx.lineTo(this.size[0], y);
    }

    this.ctx.strokeStyle = "darkgrey";
    this.ctx.stroke();
 

    
    this.ctx.lineWidth = 5;
    this.ctx.strokeStyle = 'black';

    //trazo de Y
    this.ctx.beginPath();
    this.ctx.moveTo(30, 30);
    this.ctx.lineTo(30,570);
    this.ctx.stroke();
    
    //flecha de Y
    this.ctx.beginPath();
    this.ctx.moveTo(30, 10);
    this.ctx.lineTo(20, 30);
    this.ctx.lineTo(40, 30);
    this.ctx.fill();

    //letra Y
    this.ctx.font = "20px Verdana";
    this.ctx.textBaseline = 'middle';
    this.ctx.textAlign = 'center';
    this.ctx.fillText("y", 10, 15);


     //trazo de X
     this.ctx.beginPath();
     this.ctx.moveTo(28, 570);
     this.ctx.lineTo(580,570);
     this.ctx.stroke();
     
     //flecha de X
     this.ctx.beginPath();
     this.ctx.moveTo(590, 570);
     this.ctx.lineTo(570, 560);
     this.ctx.lineTo(570, 580);
     this.ctx.fill();

    //letra X
     this.ctx.font = "20px Verdana";
     this.ctx.textBaseline = 'middle';
     this.ctx.textAlign = 'center';
     this.ctx.fillText("x", 590, 580);
      

     let num = 0;
     for (let x = 80; x <= this.size[0] -10; x = x + 40) {
        num = num +1;
      this.ctx.font = "20px Verdana";
      this.ctx.textBaseline = 'middle';
      this.ctx.textAlign = 'center';
      this.ctx.fillText(num.toString(), x, 590);
    }

    for (let y = 80; y <= this.size[1] -20; y = y + 40) {
    num = num -1;
    this.ctx.font = "20px Verdana";
    this.ctx.textBaseline = 'middle';
    this.ctx.textAlign = 'center';
    this.ctx.fillText(num.toString(), 10, y);
    this.ctx.restore();
    }
  }

  ajusta(xx, yy) {
    const posCanvas = this.ctx.canvas.getBoundingClientRect();

    const x = xx - posCanvas.left;
    const y = yy - posCanvas.top;

    return {
      x,
      y
    };
  }

  selecciona(e) {

    const pos = this.ajusta(e.clientX, e.clientY);
    const x = pos.x;
    const y = pos.y;
    let borrado = false;
    let palitoAgragado = false;




    this.numero.forEach(element => {
      if (element.comprueba(x, y)) {
        if (element.valido === true) {
          element.valido = false;
          borrado = true;
        }
      }
    });

    if (borrado === false) {
      this.numero.push(new Numero(this.numero.length, x, y, 20, 20, 7, this.ctx));

      console.log(this.numero);

    }
    this.resetCanvas();

    console.log(x, y);
  }



  resetCanvas() {
    this.ctx.save();
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.cuadricula();
    this.dibujarLimites();
    this.numero.forEach(element => {
      element.dibuja();
    });
    this.ctx.restore();
  }

  dibujarLimites() {
    this.palitos.forEach(element => {
      element.dibuja();
    });
  }



}