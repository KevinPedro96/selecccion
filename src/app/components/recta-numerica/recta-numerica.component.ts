import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';

class Numero {
  x: number;
  y: number;
  w: number;
  h: number;

  id: number;
  numero: any;
  sinEvaluar = "orange"; // 0
  correcto = "green"; //1
  incorrecto = "red"; //-1
  numeroAsignado: number = undefined;

  punto = 0;

  seleccion: false;

  ctx: CanvasRenderingContext2D;
  valido: boolean = true;

  constructor(id: number, x: number, y: number, w: number, h: number, numero: any, ctx: CanvasRenderingContext2D) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.id = id;

    this.numero = numero;

    this.ctx = ctx;
  }

  dibuja() {
    this.ctx.save();
    if (this.valido === false) {
      
      return;
    } else {
      this.ctx.fillStyle = this.punto === 0 ? this.sinEvaluar : (this.punto === 1 ? this.correcto : this.incorrecto);
      this.ctx.lineWidth = 3;
      var X = 0;
      var Y = 0;
      var R = 10;

      var L = 5;
      var paso = 2;

      var estrella = L / paso;
      var rad = (2 * Math.PI) / estrella;

      this.ctx.translate(this.x, this.y);

      // dibuja el trazado 
      this.ctx.beginPath();
      for (var i = 0; i < L; i++) {
        let x = X + R * Math.cos(rad * i);
        let y = Y + R * Math.sin(rad * i);
        this.ctx.lineTo(x, y);
      }
      this.ctx.closePath();
      this.ctx.stroke();
      this.ctx.fill();
      this.ctx.fillRect(this.x, this.y, this.w, this.h);
    }
    this.ctx.restore();
  }
  comprueba(x, y) {
  
 
    if (x > (this.x - this.w / 2) && x < (this.x - this.w / 2) + this.w && y > (this.y - this.h / 2) && y < (this.y - this.h / 2) + this.h) {
      return true;
    } else {
      return false;
    }
    
    
  }
}

class Palito {
  posX: number;
  posY: number;
  numero: number;
  h: number;
  w: number;
  numOculto: boolean = false;
  punto: boolean;
  ctx: CanvasRenderingContext2D;
  tecleadoUsuario: boolean;

  constructor(x: number, y: number, numero: number, h: number, w: number, numOculto: boolean, punto: boolean, ctx: CanvasRenderingContext2D, tecleado: boolean) {
    this.posX = x;
    this.posY = y;
    this.numero = numero;
    this.h = h;
    this.w = w;
    this.numOculto = numOculto;
    this.punto = punto;
    this.ctx = ctx;
    this.tecleadoUsuario = tecleado;
  }

  dibuja() {
    if (this.numOculto === true) {
      return;
    } else {

      this.ctx.save();
      this.ctx.lineWidth = 5;
      this.ctx.strokeStyle = 'black';

      this.ctx.beginPath();
      this.ctx.moveTo(this.posX, 200);
      this.ctx.lineTo(this.posX, 230);
      this.ctx.stroke();
      this.ctx.restore();

      this.ctx.font = "20px Verdana";
      this.ctx.textBaseline = 'middle';
      this.ctx.textAlign = 'center';
      this.ctx.fillText(this.numero.toString(), this.posX, this.posY + 45);

    }
  }
  comprueba(x, y) {
  

    if (x > (this.posX - this.w / 2) && x < (this.posX - this.w / 2) + this.w && y > (this.posY - this.h / 2) && y < (this.posY - this.h / 2) + this.h) {
      return true;
      
    } else {
      return false;
      
    }
  }


}
@Component({
  selector: 'app-recta-numerica',
  templateUrl: './recta-numerica.component.html',
  styleUrls: ['./recta-numerica.component.css']
})
export class RectaNumericaComponent implements OnInit {

  @ViewChild('canvas', {
    static: true
  })
  canvas: ElementRef < HTMLCanvasElement > ;
  private ctx: CanvasRenderingContext2D;

  @Input() data: any = {
    initLine: 1,
    endLine: 100,
    numerosAleatorios: [50,70, 15, 80, 90],
    puntos: 0,
    numero: 0
    
  };
  @Input() onlyView: boolean = true;
  @Input() viewResult: boolean;
  @Input() aksForResult: boolean;
  @Input() infoRes: any;
  @Output() dataSend: EventEmitter < any > = new EventEmitter();

  banderaEvaluacionVacio = true; 
  

  size = [750, 320];
  numero: Numero[] = [];
  palitos: Palito[] = [];
  // initLine = 0;
  // endLine = 100;
  // numerosAleatorios = [10, 50, 20, 75, 65, ];
  recta = {
    x: 50,
    y: 200,
    xx: 700,
    yy: 200
  };
  numeroSeleccionado: number;

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {

    // console.log(this.aksForResult);
    
    if (this.aksForResult) { 
      
      this.comprovarColicion();
      console.log(this.numero)
    }
  }

  ngAfterViewInit(): void {

    let numxPixel = this.data.endLine - this.data.initLine;
    let cantidadPixeles = this.recta.xx - this.recta.x;
    let numPix = cantidadPixeles / numxPixel;
    this.canvas.nativeElement.addEventListener('click', this.selecciona.bind(this), false);
    this.ctx = this.canvas.nativeElement.getContext('2d')
    if (this.ctx) {
      this.palitos.push(new Palito(this.recta.x, this.recta.y, this.data.initLine, 10, 10, false, true, this.ctx, false));
      this.palitos.push(new Palito(this.recta.xx, this.recta.yy, this.data.endLine, 10, 10, false, true, this.ctx, false));
      this.data.numerosAleatorios.forEach(element => {
        let nuevoX = numPix * element;
        this.palitos.push(new Palito(nuevoX + this.recta.x, this.recta.y, element, 10, 10, true, false, this.ctx, false));
        this.numero.push(new Numero(this.numero.length, nuevoX + this.recta.x, 200, 10, 10, element, this.ctx));
      });
      this.numeroSeleccionado = this.data.numerosAleatorios[0];
      if (this.viewResult) {
        this.palitos = [];
        this.infoRes.palito.forEach(element => {
          this.palitos.push(new Palito(element.posX, element.posY, element.numero, element.h, element.w, element.numOculto, element.punto, this.ctx, true));           
        });
        
      
        this.infoRes.numeros.forEach(element => {
          // let newNumero = new Numero(element.id, element.x,element.y, element.w, element.h, element.numero, this.ctx);
          // newNumero.valido = element.valido;
          // newNumero.punto = element.punto;
          // newNumero.numeroAsignado = element.numeroAsignado;
          // this.numero.push(newNumero);

          this.numero.forEach(busqueda => {
            if(element.id === busqueda.id){
              busqueda.punto = element.punto;
            }
          });
          
        });

  
      //   this.numero.forEach(element => {
      //     if (element.numeroAsignado && element.numeroAsignado === element.numero) {
      //       element.punto = 1;
  
      //     } else {
      //       element.punto = -1;
      //     }
      //   });
      }
      this.resetCanvas();
    }
  
    
    
  }

  ngOnInit() {

  }


  ajusta(xx, yy) {
    const posCanvas = this.ctx.canvas.getBoundingClientRect();

    const x = xx - posCanvas.left;
    const y = yy - posCanvas.top;

    return {
      x,
      y
    };
  }

  selecciona(e) {

    if(this.viewResult){return}
    const pos = this.ajusta(e.clientX, e.clientY);
    const x = pos.x;
    const y = pos.y;
    let borrado = false;
    let palitoAgragado = false;

    this.palitos.forEach(element => {

      if (element.numero === this.numeroSeleccionado && element.numOculto == false) {
        palitoAgragado = true;
        element.posX = x;
        this.numero.forEach(element2 => {
          if(element2.numeroAsignado === this.numeroSeleccionado){
            element2.numeroAsignado = undefined;
          }
          if (element2.comprueba(x, this.recta.y)) {
            element2.numeroAsignado = this.numeroSeleccionado;
          }
  
        });

      }
    });
    // this.numero.forEach(element => {
    //   if (element.comprueba(x, y)) {
    //     if(element.valido=== true){
    //       element.valido = false;
    //       borrado = true;
    //     }
    //   }
    // });

    if (palitoAgragado === false) {
      this.palitos.push(new Palito(x, this.recta.y, this.numeroSeleccionado, 10, 10, false, false, this.ctx, true));
      this.numero.forEach(element => {
        if (element.comprueba(x, this.recta.y)) {
          element.numeroAsignado = this.numeroSeleccionado;
        }

      });
        console.log(this.numero);
        
        
    }

    // if(borrado === false){
    //   this.numero.push(new Numero(this.numero.length, x, 200, 20, 20, 7, this.ctx));




    // }

    
    
    this.resetCanvas();

  }

  creacionRecta() {
    //stilos
    this.ctx.save();
    this.ctx.lineWidth = 5;
    this.ctx.strokeStyle = 'black';

    // trazo
    this.ctx.beginPath();
    this.ctx.moveTo(this.recta.x, this.recta.y);
    this.ctx.lineTo(this.recta.xx, this.recta.yy);
    this.ctx.stroke();

    //Trazar flechas
    this.ctx.beginPath();
    this.ctx.moveTo(20, 200);
    this.ctx.lineTo(50, 183);
    this.ctx.lineTo(50, 218);
    this.ctx.fill();

    this.ctx.beginPath();
    this.ctx.moveTo(730, 200);
    this.ctx.lineTo(700, 183);
    this.ctx.lineTo(700, 218);
    this.ctx.fill();

    this.ctx.restore();
  }

  resetCanvas() {
    this.ctx.save();
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    // this.cuadricula();
    this.creacionRecta();
    this.dibujarLimites();

    this.numero.forEach(element => {
      element.dibuja();
    });

    this.ctx.restore();
  }

  dibujarLimites() {
    this.palitos.forEach(element => {
      element.dibuja();
            
    });
  }

  comprovarColicion() {
    let key = 0;
    
    for (let item of this.palitos){
      
      if(item.tecleadoUsuario){
        key  ++;  
      } 
   
    }          
    console.log("key", key, this.numero.length);
    
    if(key === this.numero.length){
      this.banderaEvaluacionVacio = false;
    }
    
    if(this.banderaEvaluacionVacio === true){
      
      this.banderaEvaluacionVacio = true;
      this.dataSend.emit(false);

    }else{
    
    if (!this.viewResult) {
      
      let puntos = 0
      let total = this.numero.length
        
      for (const element of this.numero) {
        // if(element.numeroAsignado === undefined){
        //   this.banderaEvaluacionVacio = true;
        //   break;
        // }
        if (element.numeroAsignado === element.numero) {
          puntos++;
          element.punto = 1;

        } else {
          element.punto = -1;
          this.palitos.push(new Palito(element.x, element.y-100, element.numero, 10, 10, false, true, this.ctx, false));
        }      
      }
        // if(this.banderaEvaluacionVacio){
        //   localStorage.setItem("Recta", JSON.stringify(this.numero));
        //   this.dataSend.emit(false);
         
        // }else{
          this.data.puntos = puntos + "/" + total;
          this.data.total = total;
          this.data.numeros = this.numero;
          this.data.palito = this.palitos;
          this.dataSend.emit(this.data);
       
    
    } else {


    }

    this.resetCanvas();

    if (this.viewResult === true) {
      this.numero.forEach(element => {
        element.punto;
      });
    }
    }
  }  

}
