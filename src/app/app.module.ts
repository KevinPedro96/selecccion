import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AsociarIdentificadorComponent } from './components/asociar-identificador/asociar-identificador.component';
import { FormsModule } from '@angular/forms';
import { AsociarIfentificadorPrevComponent } from './components/asociar-ifentificador-prev/asociar-ifentificador-prev.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input'
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { RectaNumericaComponent } from './components/recta-numerica/recta-numerica.component';
import {MatCardModule} from '@angular/material/card';
import {MatSliderModule} from '@angular/material/slider';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { RompezabezasComponent } from './components/rompezabezas/rompezabezas.component';
import { RectaNumerica2Component } from './components/recta-numerica2/recta-numerica2.component';
import {MatButtonModule} from '@angular/material/button';
import { PlanoCartesianoComponent } from './components/plano-cartesiano/plano-cartesiano.component';
import { PlanoCartesianoPhaserComponent } from './components/plano-cartesiano-phaser/plano-cartesiano-phaser.component';
import { ReglaComponent } from './components/regla/regla.component';
import { PapasComponent } from './parev/papas/papas.component';
import {MatSelectModule} from '@angular/material/select';
import { SeleccionTraignulosComponent } from './components/seleccion-traignulos/seleccion-traignulos.component';
import { GraficasComponent } from './components/graficas/graficas.component';

@NgModule({
  declarations: [
    AppComponent,
    AsociarIdentificadorComponent,
    AsociarIfentificadorPrevComponent,
    RectaNumericaComponent,
    RompezabezasComponent,
    RectaNumerica2Component,
    PlanoCartesianoComponent,
    PlanoCartesianoPhaserComponent,
    ReglaComponent,
    PapasComponent,
    SeleccionTraignulosComponent,
    GraficasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatCardModule,
    MatSliderModule,
    MatCheckboxModule,
    DragDropModule,
    MatButtonModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
